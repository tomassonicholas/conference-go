import * as snips from './modules/modules.js'


window.addEventListener('DOMContentLoaded', async () =>{
    const url = 'http://localhost:8000/api/states/'

    const data = await snips.getData(url)
    const selectStateTag = document.getElementById('state')


    for (let state of data.states ){
        let newOp = document.createElement("option")
        newOp.innerHTML = `${state.name}`
        newOp.setAttribute('value', `${state.abbreviation}`)
        selectStateTag.appendChild(newOp)
    }

    const formTag = document.getElementById('create-location-form')


    formTag.addEventListener('submit', async event =>{
        event.preventDefault();
        const formData = new FormData(formTag)
        const json = JSON.stringify(Object.fromEntries(formData))
        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: "POST",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newLocation = await response.json();
        }

    });

});
