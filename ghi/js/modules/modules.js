async function getData(url){
    try {
        let response = await fetch(url);

        if (!response.ok){
            return `
            <div class="alert alert-danger" role="alert">
                Bad JSON response.
            </div>
        `
        } else {
            let data = await response.json()
            return data
        }
    } catch (urlEvent) {
        return `
            <div class="alert alert-danger" role="alert">
                Bad URL fetch.
            </div>
        `
    }
};


function createPlaceholder(){
    return `
        <div class="card shadow mb-3" aria-hidden="true">
        <img src="..." class="card-img-top" alt="...">
            <div class="card-body">
            <h5 class="card-title placeholder-glow">
                <span class="placeholder col-6"></span>
            </h5>
            <p class="card-text placeholder-glow">
                <span class="placeholder col-7"></span>
                <span class="placeholder col-4"></span>
                <span class="placeholder col-4"></span>
                <span class="placeholder col-6"></span>
                <span class="placeholder col-8"></span>
            </p>
            </div>
            <div class="card-footer">
                <span class="placeholder col-4"></span>
            </div>
        </div>
    `;
}

export {getData, createPlaceholder}
